# show4me/s3-cache-cleaner:latest
FROM show4me/alpine:3.9
ARG CACHE_DIR="/tmp/s3cache"
USER root

COPY /s3_delcache.sh /bin/
COPY /run.sh /bin/


RUN chmod u+x /bin/run.sh &&  chmod u+x /bin/s3_delcache.sh
RUN sh -c "apk update && apk add vim"

CMD ["/bin/run.sh"]

