# s3-cache-cleaner

Build

    docker build \
    --tag=show4me/s3-cache-cleaner:latest \
    .

Run as stand-alone container

    docker run \
    --env CACHE_LIMIT=52428800 \
    --env BUCKET_NAME="show4me-upload-beta" \
    --env CRON_PERIOD="*/15 * * * *" \
    --mount type=bind,source=/mnt/s3/cache,target=/tmp/s3cache  \
    --detach \
    --name=beta-s3fs-cache-cleaner \
    show4me/s3-cache-cleaner:latest
    
    
Notes: 
- CACHE_LIMIT - set an integer value in **kilo-bytes**. Default is 52428800 (50 Gb).
- CRON_PERIOD - a crontab-based pattern to start cache cleaner script. 
- Mount the directory inside container that mounted from host. It is "/tmp/s3cache" inside a container.

Run as a docker service

    docker service create \
    --name=beta-s3fs-cache-cleaner \
    --env CACHE_LIMIT=52428800 \
    --env BUCKET_NAME="show4me-upload-beta" \
    --env CRON_PERIOD="*/15 * * * *" \
    --mount type=bind,source=/etc/localtime,target=/etc/localtime,readonly \
    --mount type=bind,source=/mnt/s3/cache,target=/tmp/s3cache  \
    show4me/s3-cache-cleaner:latest