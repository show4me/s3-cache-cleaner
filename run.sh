#!/bin/sh

SCRIPT_NAME="s3_delcache.sh"
CACHE_DIR="/tmp/s3cache"
BUCKET_NAME=${BUCKET_NAME:-"show4me-upload-beta"}
CACHE_LIMIT=${CACHE_LIMIT:-"500"}
CRON_PERIOD=${CRON_PERIOD:-"*/15 * * * *"}

echo "${CRON_PERIOD} /bin/${SCRIPT_NAME} ${BUCKET_NAME} ${CACHE_DIR} ${CACHE_LIMIT} " | crontab -

crontab -l

crond -f